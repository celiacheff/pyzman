#!/usr/bin/python

# Lib to Convert between Gregorian and Jewish dates
# Shortcuts :
#       gregorian_to_hebrew(year, month, day)
#       hebrew_to_gregorian(year, month, day)
# Return a tuple of (year, month, day)

# Constants
_HEBREW_EPOCH = 347995.5
_GREGORIAN_EPOCH = 1721425.5


def hebrew_leap(h_year):
    """Is a given Hebrew year a leap year ?"""

    return (((h_year * 7) + 1) % 19) < 7


def hebrew_year_months(h_year):
    """
    How many months are there in a Hebrew year
    (12 = normal, 13 = leap)
    """

    if hebrew_leap(h_year):
        return 13
    else:
        return 12


def _hebrew_delay_1(h_year):
    """Test for delay of start of new year and to avoid"""

    #  Sunday, Wednesday, and Friday as start of the new year.
    months = ((235 * h_year) - 234) // 19
    parts = 12084 + (13753 * months)
    day = (months * 29) + parts // 25920

    if ((3 * (day + 1)) % 7) < 3:
        day += 1

    return day


def _hebrew_delay_2(h_year):
    """Check for delay in start of new year due to len of adjacent years"""

    last = _hebrew_delay_1(h_year - 1)
    present = _hebrew_delay_1(h_year)
    next = _hebrew_delay_1(h_year + 1)

    if next - present == 356:
        return 2
    elif present - last == 382:
        return 1
    else:
        return 0


def hebrew_year_days(h_year):
    """ Return the numbers of days for a given year """

    return (hebrew_to_jd(h_year + 1, 7, 1) -
            hebrew_to_jd(h_year, 7, 1))


def hebrew_month_days(h_year, h_month):
    """ Return day's number of a given month of a year """

    # First of all, dispose of fixed-length 29 day months
    if h_month in (2, 4, 6, 10, 13):
        return 29

    # If it's not a leap year, Adar has 29 days
    if h_month == 12 and not hebrew_leap(h_year):
        return 29

    # If it's Heshvan, days depend on length of year
    if h_month == 8 and (hebrew_year_days(h_year) % 10) != 5:
        return 29

    # Similarly, Kislev varies with the length of year
    if h_month == 9 and (hebrew_year_days(h_year) % 10) == 3:
        return 29

    # Nope, it's a 30 day month
    return 30


def hebrew_to_jd(h_year, h_month, h_day):
    """ Convert a hebrew date to jd """

    jd = (_HEBREW_EPOCH + _hebrew_delay_1(h_year) +
          _hebrew_delay_2(h_year) + h_day + 1)
    months = hebrew_year_months(h_year)

    if h_month < 7:
        for m in range(7, months + 1):
            jd += hebrew_month_days(h_year, m)
        for m in range(1, h_month):
            jd += hebrew_month_days(h_year, m)
    else:
        for m in range(7, h_month):
            jd += hebrew_month_days(h_year, m)

    return jd


def jd_to_hebrew(jd):
    """ Convert a jd time to Hebrew date """

    jd = int(jd) + 0.5
    count = int(((jd - _HEBREW_EPOCH) * 98496.0) // 35975351.0)

    year = count - 1
    i = count

    while jd >= hebrew_to_jd(i, 7, 1):
        i += 1
        year += 1

    if jd < hebrew_to_jd(year, 1, 1):
        first = 7
    else:
        first = 1

    i = month = first
    while jd > hebrew_to_jd(year, i, hebrew_month_days(year, i)):
        i += 1
        month += 1

    day = int(jd - hebrew_to_jd(year, month, 1)) + 1

    return (year, month, day)


def gregorian_to_jd(g_year, g_month, g_day):
    """ Convert a gregorian date to jd """

    if g_month <= 2:
        leap_adj = 0
    elif gregorian_leap(g_year):
        leap_adj = -1
    else:
        leap_adj = -2

    return (_GREGORIAN_EPOCH - 1 +
            (365 * (g_year - 1)) +
            (g_year - 1) // 4 -
            (g_year - 1) // 100 +
            (g_year - 1) // 400 +
            (((367 * g_month) - 362) // 12 + leap_adj + g_day))


def gregorian_leap(g_year):
    """ Check for gregorian leap year """

    if (((g_year % 4) == 0) and ((g_year % 400) != 100) and
            ((g_year % 400) != 200) and ((g_year % 400) != 300)):
        return True
    else:
        return False


def gregorian_month_last_day(g_year, g_month):
    """ Get the number of day of a month """

    if (gregorian_leap(g_year)) and g_month == 2:
        return 29
    else:
        lengths = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        return lengths[g_month-1]


def jd_to_gregorian(jd):
    """ Return Gregorian date in a (Y, M, D) tuple """

    wjd = int(jd - 0.5) + 0.5
    depoch = wjd - _GREGORIAN_EPOCH
    quadricent = depoch // 146097
    dqc = depoch % 146097
    cent = dqc // 36524
    dcent = dqc % 36524
    quad = dcent // 1461
    dquad = dcent % 1461
    yindex = dquad // 365
    year = int((quadricent * 400) + (cent * 100) + (quad * 4) + yindex)
    if not (cent == 4 or yindex == 4):
        year += 1
    yearday = wjd - gregorian_to_jd(year, 1, 1)
    if wjd < gregorian_to_jd(year, 3, 1):
        leap_adj = 0
    elif gregorian_leap(year):
        leap_adj = 1
    else:
        leap_adj = 2

    month = int((((yearday + leap_adj) * 12) + 373) // 367)

    day = int(wjd - gregorian_to_jd(year, month, 1)) + 1

    return (year, month, day)


# Commodity functions
def gregorian_to_hebrew(g_year, g_month, g_day):
    """ Shortcut - convert gregorian to hebrew """

    jd = gregorian_to_jd(g_year, g_month, g_day)
    hebrew = jd_to_hebrew(jd)
    return hebrew


def hebrew_to_gregorian(h_year, h_month, h_day):
    """ Shortcut - convert hebrew to gregorian """

    jd = hebrew_to_jd(h_year, h_month, h_day)
    gregorian = jd_to_gregorian(jd)
    return gregorian
