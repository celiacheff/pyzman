import datelib
from strings import paracha_name

import calendar

sat_short = [-1, 52, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
             15, 16, 17, 18, 19, 20, 53, 23, 24, -1, 25, 54, 55, 30, 56, 33,
             34, 35, 36, 37, 38, 39, 40, 58, 43, 44, 45, 46, 47, 48, 49, 50]

sat_long = [-1, 52, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
            15, 16, 17, 18, 19, 20, 53, 23, 24, -1, 25, 54, 55, 30, 56, 33, 34,
            35, 36, 37, 38, 39, 40, 58, 43, 44, 45, 46, 47, 48, 49, 59]

mon_short = [51, 52, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
             16, 17, 18, 19, 20, 53, 23, 24, -1, 25, 54, 55, 30, 56, 33, 34,
             35, 36, 37, 38, 39, 40, 58, 43, 44, 45, 46, 47, 48, 49, 59]

mon_long = [51, 52, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
            16, 17, 18, 19, 20, 53, 23, 24, -1, 25, 54, 55, 30, 56, 33, -1, 34,
            35, 36, 37, 57, 40, 58, 43, 44, 45, 46, 47, 48, 49, 59]

thu_normal = [52, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
              16, 17, 18, 19, 20, 53, 23, 24, -1, -1, 25, 54, 55, 30, 56, 33,
              34, 35, 36, 37, 38, 39, 40, 58, 43, 44, 45, 46, 47, 48, 49, 50]

thu_normal_israel = [52, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                     14, 15, 16, 17, 18, 19, 20, 53, 23, 24, -1, 25, 54, 55,
                     30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 58, 43, 44,
                     45, 46, 47, 48, 49, 50]

thu_long = [52, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
            16, 17, 18, 19, 20, 21, 22, 23, 24, -1, 25, 54, 55, 30, 56, 33, 34,
            35, 36, 37, 38, 39, 40, 58, 43, 44, 45, 46, 47, 48, 49, 50]

sat_short_leap = [-1, 52, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                  14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, -1,
                  28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 58, 43,
                  44, 45, 46, 47, 48, 49, 59]

sat_long_leap = [-1, 52, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, -1,
                 28, 29, 30, 31, 32, 33, -1, 34, 35, 36, 37, 57, 40, 58, 43,
                 44, 45, 46, 47, 48, 49, 59]

mon_short_leap = [51, 52, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                  15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, -1, 28,
                  29, 30, 31, 32, 33, -1, 34, 35, 36, 37, 57, 40, 58, 43, 44,
                  45, 46, 47, 48, 49, 59]

mon_short_leap_israel = [51, 52, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
                         13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
                         26, 27, -1, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37,
                         38, 39, 40, 58, 43, 44, 45, 46, 47, 48, 49, 59]

mon_long_leap = [51, 52, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, -1, -1,
                 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 58, 43,
                 44, 45, 46, 47, 48, 49, 50]

mon_long_leap_israel = [51, 52, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
                        13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
                        27, -1, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
                        40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50]

thu_short_leap = [52, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                  15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, -1,
                  29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43,
                  44, 45, 46, 47, 48, 49, 50]

thu_long_leap = [52, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, -1,
                 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43,
                 44, 45, 46, 47, 48, 49, 59]


def kevia(year):
    """ Get kevia for a year """
    k = datelib.hebrew_year_days(year)
    chechvan_long = ((k % 10) == 5)
    kislev_short = ((k % 10) == 3)
    if chechvan_long and (not kislev_short):
        return 2
    elif (not chechvan_long) and kislev_short:
        return 0
    else:
        return 1


def get_parasha(year, month, day, in_israel=True):
    """ Return parasha reading for a date """
    current = datelib.hebrew_to_jd(year, month, day)
    current_day = (int(current) + 2) % 7  # TODO: What's wrong here -_- ?

    # if today is not Shabbos, then there is no normal parsha reading. If
    # commented our will return LAST week's parsha for a non shabbos
    if (current_day != 6):
        return ""
        #return -1

    # // kvia = whether a Jewish year is short/regular/long (0/1/2)
    # // roshHashana = Rosh Hashana of this Jewish year
    # // roshHashanaDay= day of week Rosh Hashana was on this year
    # // week= current week in Jewish calendar from Rosh Hashana
    # // array= the correct index array for this Jewish year
    # // index= the index number of the parsha name
    kvia = kevia(year)
    # int roshHashanaDay;
    # int week;
    # int[] array = null;
    # int index;

    rosh_hashana = datelib.hebrew_to_jd(year, 7, 1)
    # rosh_hashana = datelib.gregorian_to_jd(year, 7, 1)
    # get day Rosh Hashana was on
    rosh_hashana_day = int(rosh_hashana + 2) % 7  # TODO: What's wrong here -_- ?

    leap = datelib.hebrew_leap(year)

    # week is the week since the first Shabbos on or after Rosh Hashana
    week = int(((current - rosh_hashana) - (6 - rosh_hashana_day))) // 7

    array = None
    # determine appropriate array
    if not leap:
        if rosh_hashana_day == 6:
            # RH was on a Saturday
            if kvia == 0:
                array = sat_short
            elif kvia == 2:
                array = sat_long
        elif rosh_hashana_day == 1:  # RH was on a Monday
            if kvia == 0:
                array = mon_short
            elif kvia == 2:
                array = mon_short if in_israel else mon_long
        elif rosh_hashana_day == 2:  # RH was on a Tuesday
            if kvia == 1:
                array = mon_short if in_israel else mon_long
        elif rosh_hashana_day == 4:  # RH was on a Thursday
            if (kvia == 1):
                array = thu_normal_israel if in_israel else thu_normal
            elif kvia == 2:
                array = thu_long
    else:  # if leap year
        if rosh_hashana_day == 6:  # RH was on a Sat
            if kvia == 0:
                array = sat_short_leap
            elif kvia == 2:
                array = sat_short_leap if in_israel else sat_long_leap
        elif rosh_hashana_day == 1:  # RH was on a Mon
            if kvia == 0:
                array = mon_short_leap_israel if in_israel else mon_short_leap
            elif kvia == 2:
                array = mon_long_leap_israel if in_israel else mon_long_leap
        elif rosh_hashana_day == 2:  # RH was on a Tue
            if kvia == 1:
                array = mon_long_leap_israel if in_israel else mon_long_leap
        elif rosh_hashana_day == 4:  # RH was on a Thu
            if kvia == 0:
                array = thu_short_leap
            elif kvia == 2:
                array = thu_long_leap

    # if something goes wrong
    if array is None:
        raise Exception("Error getting array")

    # get index from array
    index = array[week]

    # If no Parsha this week
    if index == -1:
        return ""

    # // if parsha this week
    # // else {
    # // if (getDayOfWeek() != 7){//in weekday return next shabbos's parsha
    # // System.out.print(" index=" + index + " ");
    # // return parshios[index + 1];
    # // this code returns odd data for yom tov. See parshas kedoshim display
    # // for 2011 for example. It will also break for Sept 25, 2011 where it
    # // goes one beyong the index of Nitzavim-Vayelech
    # // }
    return paracha_name(index)
    #return index;

if __name__ == '__main__':
    # y, m, d = [5775, 8, 22]
    # gy = 2014

    # for gm in range(1, 12):
    #     for gd in range(1, calendar.mdays[gm]):
    #         y, m, d = datelib.gregorian_to_hebrew(gy, gm, gd)
    #         print(y, m, d)
    #         g = get_parasha(y, m, d)
    #         try:
    #             print(g)
    #         except Exception as e:
    #             print(e)
    g = get_parasha(5775, 10, 19)
    y, m, d = datelib.hebrew_to_gregorian(5775, 10, 19)
    print(y, m, d)
    print(g)
