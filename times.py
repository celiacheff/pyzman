#!/usr/bin/python

import math
import arrow


class Time:

    # Class constants
    _refraction = 34.478885263888294 / 60.0
    _solarRadius = 16.0 / 60.0
    _earthRadius = 6356.9  # Km

    # The number of degr of longitude that corresponds to one hour time diff
    _DEG_PER_HOUR = 360.0 / 24.0

    def __init__(self, year, month, day, location):
        self.year = date.year
        self.month = date.month
        self.day = date.day
        self.location = location

    def day_of_year(self):
        """ Compute the day of year of a gregorian date """
        n1 = (275 * self.month) / 9
        n2 = (self.month + 9) / 12
        n3 = (1 + ((self.year - 4 * (self.year / 4) + 2) / 3))
        return n1 - (n2 * n3) + self.day - 30

    def elevation_adjustment(self, elevation):
        """ Adjust elevation for zenith """
        return math.degrees(math.acos(self._earthRadius /
                                      (self._earthRadius +
                                       (elevation / 1000))))

    def adjust_zenith(self, zenith, elevation):
        """ Adjust zenith value only if surnise or sunset, and adjust elevation
        """
        # only adj if it's exactly sr or ss
        if (zenith == 90):
            return (zenith + self._solarRadius + self._refraction +
                    self.elevation_adjustment(elevation))
        else:
            return zenith

    def get_suntime_utc(self,
                        zenith=90,
                        adjust_elevation=True,
                        is_sunrise=True):
        """ Get sunrise or sunset in utc value """

        elevation = self.location.elevation if adjust_elevation else 0
        adjusted_zenith = self.adjust_zenith(zenith, elevation)

        # step 1: First calculate the day of the year
        doy = self.day_of_year()

        # step 2: convert the long to hour value and calculate an approx time
        long_hour = self.location.longitude / self._DEG_PER_HOUR

        if is_sunrise:
            t = doy + ((6 - long_hour) / 24)
        else:
            t = doy + ((18 - long_hour) / 24)

        # step 3: calculate the sun's mean anomaly
        m = (0.9856 * t) - 3.289

        # step 4: calculate the sun's true longitude
        l = (m +
             (1.916 * math.sin(math.radians(m))) +
             (0.020 * math.sin(math.radians(2 * m))) +
             282.634)

        while (l < 0):
            l += 360

        while (l >= 360):
            l -= 360

        # step 5a: calculate the sun's right ascension
        ra = math.degrees(math.atan(0.91764 * math.tan(math.radians(l))))

        while (ra < 0):
            ra += 360

        while (ra >= 360):
            ra += 360

        # step 5b: right ascension value needs to be in the same quadrant as L
        l_quadrant = math.floor(l / 90) * 90
        ra_quadrant = math.floor(ra / 90) * 90
        ra = ra + (l_quadrant - ra_quadrant)

        # step 5c: right ascension value needs to be converted into hours
        ra /= 15

        # step 6: calculate the sun's declination
        sin_dec = 0.39782 * math.sin(math.radians(l))
        cos_dec = math.cos(math.asin(sin_dec))

        # step 7a: calculate the sun's local hour angle
        cos_h = (math.cos(math.radians(adjusted_zenith)) -
                 (sin_dec * math.sin(math.radians(self.location.latitude))) /
                 (cos_dec * math.cos(math.radians(self.location.latitude))))

        # step 7b: finish calculating H and convert into hours
        if is_sunrise:
            h = 360 - math.degrees(math.acos(cos_h))
        else:
            h = math.degrees(math.acos(cos_h))

        h = h / 15

        # step 8: calculate local mean time

        local_time = h + ra - (0.06571 * t) - 6.622

        # step 9: convert to UTC
        time = local_time - long_hour
        while (time < 0):
            time += 24

        while (time >= 24):
            time -= 24

        return time

    def format(self, time, tz='utc'):
        """ Convert time to h-m-s-mm """
        # if math.isnan(time):
        #     return None

        date = arrow.Arrow(year=self.year, month=self.month, day=self.day)

        hours = int(time)  # retain only the hours
        time -= hours
        time *= 60
        minutes = int(time)  # retain only the minutes
        time -= minutes
        time *= 60
        seconds = int(time)  # retain only the seconds
        time -= seconds  # remaining milliseconds
        milliseconds = int(time * 1000)

        date = date.replace(hour=hours,
                            minute=minutes,
                            second=seconds,
                            microsecond=milliseconds)
        return date.to(tz)

    #################          Utc calculations       #########################

    def sunrise_utc(self, zenith=90):
        """ Get sunrise with elevation """
        sr = self.get_suntime_utc(zenith, True, True)
        return self.format(sr)

    def sunrise_sea_utc(self, zenith=90):
        """ Get sunrise at sea level """
        sr = self.get_suntime_utc(zenith, False, True)
        return self.format(sr)

    def sunset_utc(self, zenith=90):
        """ Get sunset with elevation """
        sr = self.get_suntime_utc(zenith, True, False)
        return self.format(sr)

    def sunset_sea_utc(self, zenith=90):
        """ Get sunset at sea level """
        ss = self.get_suntime_utc(zenith, False, False)
        return self.format(ss)

    def temporal_hour(self, start_day, end_day):
        """ Get temporal hour, aka shaa zmanit """
        return (end_day - start_day) / 12


# Test
class Location:

    """docstring for location"""

    def __init__(self, latitude, longitude, elevation, timezone):
        super().__init__()
        self.longitude = longitude
        self.latitude = latitude
        self.elevation = elevation
        self.timezone = timezone


class Date:

    def __init__(self, year, month, day):
        super().__init__()
        self.year = year
        self.month = month
        self.day = day

if __name__ == '__main__':
    # location = Location(48.87, 2.66, 32.54, 'Europe/Paris')

    location = Location(31.76832, 35.21371, 779.46, 'Asia/Jerusalem')

    date = Date(2015, 1, 4)

    t = Time(date, location)
    print("sunrise ", t.sunrise_utc().to('Asia/Jerusalem'))
    print("sunrise sea ", t.sunrise_sea_utc().to('Asia/Jerusalem'))
    print("sunset ", t.sunset_utc().to('Asia/Jerusalem'))
    print("sunset sea ", t.sunset_sea_utc().to('Asia/Jerusalem'))
    # print("sunrise ", t.sunrise_utc().to('Europe/Paris'))
    # print("sunrise sea ", t.sunrise_sea_utc().to('Europe/Paris'))
    # print("sunset ", t.sunset_utc().to('Europe/Paris'))
    # print("sunset sea ", t.sunset_sea_utc().to('Europe/Paris'))
