#!/usr/bin/env python

import datelib


def get_holiday(year, month, day, diaspora=False, modern=False):
    """ Get holiday for a date """

    week_day = int(datelib.hebrew_to_jd(year, month, day)) % 7
    long_kislev = datelib.hebrew_month_days(year, month) == 30
    leap = datelib.hebrew_leap(year)

    # Nissan
    if month == 1:
        # Pessah
        if day == 14:
            return "Erev Pessah"
        elif day == 15:
            if not diaspora:
                return "Pessah"
            else:
                return "Pessah I"
        elif day == 16:
            if not diaspora:
                return "'Hol Hamoed Pessah"
            else:
                return "Pessah II"
        elif day in [17, 18, 19, 20]:
            return "'Hol Hamoed Pessah"
        elif day == 21:
            return "Pessah VII"
        elif day == 22:
            if diaspora:
                return "Pesach VIII"

        # Yom Hashoa
        if modern:
            # pep8 e125 hack
            if (((day == 26 and week_day == 5) or
                 (day == 28 and week_day == 1) or
                 (day == 27 and week_day == 3) or
                 (day == 27 and week_day == 5))):
                    return "Yom hashoa"

    # Iyar
    if month == 2:
        # Yom Hazikaron
        if modern:
            if (((day == 4 and week_day == 3) or
                 (day == 2 and week_day == 4) or
                 (day == 3 and week_day == 4) or
                 (day == 5 and week_day == 2))):
                    return "Yom Hazikaron"

        # Yom Haatsmaout
        # If 5 Iyar falls on Wed Yom Haatzmaut is that day.
        # If it fal1s on Friday or Shabbos it is moved back to Thursday.
        # If it falls on Monday it is moved to Tuesday
        if modern:
            if (((day == 5 and week_day == 4) or
                 (day == 3 and week_day == 5) or
                 (day == 4 and week_day == 5) or
                 (day == 6 and week_day == 3))):
                    return "Yom Haatsmout"

        # Pessah cheni
        if day == 14:
            return "Pessah Cheni"

        if modern and day == 28:
            return "Yom Yeroushalaim"

    # Sivan
    if month == 3:
        # Shavouot
        if day == 5:
            return "Erev Shavouot"
        elif day == 6:
            if not diaspora:
                return "Shavouot"
            else:
                return "Shavouot I"
        elif day == 7 and diaspora:
            return "Shavouot II"

    # Tammouz
    if month == 4:
        # Jeune 17 Tammouz
        # Push off the fast day if it falls on Shabbos
        if (((day == 17 and week_day != 7) or
             (day == 18 and week_day == 1))):
                return "17 Tammouz"

    # Av
    if month == 5:
        # 9 av
        # if Tisha Bav falls on Shabbos, push off until Sunday
        if (((day == 10 and week_day == 1) or
             (day == 9 and week_day != 7))):
                return "Ticha Beav"
        elif day == 15:
            return "Tou Beav"

    # Eloul
    if month == 6:
        if day == 29:
            return "Erev Roch Hachana"

    # Tichri
    if month == 7:
        if day == 1:
            return "Roch Hachana I"
        elif day == 2:
            return "Roch Hachana II"
        elif (day == 3 and week_day != 7) or (day == 4 and week_day == 1):
            return "Tsom Gedalia"
        elif day == 9:
            return "Erev Yom Kippour"
        elif day == 10:
            return "Yom Kippour"
        elif day == 14:
            return "Erev Soukkot"

        elif day == 15:
            if not diaspora:
                return "Soukkot"
            else:
                return "Soukkot I"
        elif day == 16:
            if not diaspora:
                return "'Hol Hamoed Soukkot"
            else:
                return "Pessah II"
        elif day in [17, 18, 19, 20]:
            return "'Hol Hamoed Soukkot"
        elif day == 21:
            return "Hoshana Raba"
        elif day == 22:
            return "Shemini Atseret"
        elif day == 23:
            if diaspora:
                return "Simha Torah"

    # Heshvan
    # Kislev
    if month == 9:
        if day == 25:
            return "Hannouka 1"
        elif day == 26:
            return "Hannouka 2"
        elif day == 27:
            return "Hannouka 3"
        elif day == 28:
            return "Hannouka 4"
        elif day == 29:
            return "Hannouka 5"
        elif day == 30 and long_kislev:
            return "Hannouka 6"

    # Tevet
    if month == 10:
        if day == 1:
            if long_kislev:
                return "Hannouka 7"
            else:
                return "Hannouka 6"
        elif day == 2:
            if long_kislev:
                return "Hannouka 8"
            else:
                return "Hannouka 7"
        elif day == 3:
            if not long_kislev:
                return "Hannouka 8"
        elif day == 10:
            return "Asara Betevet"

    # Shevat
    if month == 11:
        if day == 15:
            return "Tou Bishvat"

    # Adar
    if month == 12:
        if not leap:
            if (((day == 11 and week_day == 5) or
                 (day == 12 and week_day == 5) or
                 (day == 13 and (week_day != 6 or week_day == 7)))):
                    return "Taanit Esther"
            if day == 14:
                return "Pourim"
            elif day == 15:
                return "Shoushan Pourim"
        else:
            if day == 14:
                return "Pourim Katan"

    # Adar II
    if month == 13:
        if (((day == 11 and week_day == 5) or
             (day == 12 and week_day == 5) or
             (day == 13 and (week_day == 6 or week_day == 7)))):
                return "Taanit Esther"
        if day == 14:
            return "Pourim"
        elif day == 15:
            return "Shoushan Pourim"

    return ""
