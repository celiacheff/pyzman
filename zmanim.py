from times import Time
import arrow

from collections import OrderedDict
import json


class Zmanim:

    GEOMETRIC_ZENITH = 90

    ZENITH_16_POINT_1 = GEOMETRIC_ZENITH + 16.1
    ZENITH_8_POINT_5 = GEOMETRIC_ZENITH + 8.5
    candleLightingOffset = 18

    def __init__(self, time, location, date=None):
        self.location = location
        self.time = time
        if date is None:
            date = arrow.now()

    def alot(self, method='16.1'):
        if method == '16.1':
            return self.time.sunrise_utc(106.1 + 3.9)
        elif method == '72':
            return self.time.sunrise_sea_utc().replace(minutes=-72)

    def tsait(self, method='8.5'):
        if method == '8.5':
            return self.time.sunset_utc(self.ZENITH_8_POINT_5)
        elif method == '72':
            return self.time.sunset_sea_utc().replace(minutes=72)

    def sof_zman_chema(self, method='mga'):
        if method == 'mga':
            shaa = self.time.temporal_hour(self.alot('72'), self.tsait('72'))
            return self.alot() + shaa*3
        elif method == 'gra':
            shaa = self.time.temporal_hour(self.time.sunrise_sea_utc(),
                                           self.time.sunset_sea_utc())
            return self.time.sunrise_sea_utc() + shaa*3

    def sof_zman_tefila(self, method='mga'):
        if method == 'mga':
            shaa = self.time.temporal_hour(self.alot('72'), self.tsait('72'))
            return self.alot() + shaa*4
        elif method == 'gra':
            shaa = self.time.temporal_hour(self.time.sunrise_sea_utc(),
                                           self.time.sunset_sea_utc())
            return self.time.sunrise_sea_utc() + shaa*4

    def hatsot(self):
        shaa = self.time.temporal_hour(self.time.sunrise_sea_utc(),
                                       self.time.sunset_sea_utc())
        return self.time.sunrise_sea_utc() + shaa*6

    def minha_gedola(self):
        shaa = self.time.temporal_hour(self.time.sunrise_sea_utc(),
                                       self.time.sunset_sea_utc())
        return self.alot() + shaa*6.5

    def minha_ketana(self):
        shaa = self.time.temporal_hour(self.time.sunrise_sea_utc(),
                                       self.time.sunset_sea_utc())
        return self.alot() + shaa*9.5

    def plag(self):
        shaa = self.time.temporal_hour(self.time.sunrise_sea_utc(),
                                       self.time.sunset_sea_utc())
        return self.alot() + shaa*10.75

    def candle_lightning(self, method='18'):
        return self.time.sunset_sea_utc().replace(minutes=-18)

    def get_data(self, to_local=True, to_json=False):
        """ Get a dict of all zmanim for a day """
        if to_local:
            tz = self.location.timezone
        else:
            tz = 'utc'

        data = (('alot_16.1', self.alot().to(tz).isoformat()),
                ('alot_72', self.alot('72').to(tz).isoformat()),
                ('sof_zman_chema_mga', self.sof_zman_chema().to(tz).isoformat()),
                ('sof_zman_chema_gra', self.sof_zman_chema('gra').to(tz).isoformat()),
                ('sof_zman_tefila_mga', self.sof_zman_tefila().to(tz).isoformat()),
                ('sof_zman_tefila_gra', self.sof_zman_tefila('gra').to(tz).isoformat()),
                ('hatsot', self.hatsot().to(tz).isoformat()),
                ('minha_gedola', self.minha_gedola().to(tz).isoformat()),
                ('minha_ketana', self.minha_ketana().to(tz).isoformat()),
                ('plag', self.plag().to(tz).isoformat()),
                ('tsait_8.5', self.tsait().to(tz).isoformat()),
                ('tsait_72', self.tsait('72').to(tz).isoformat()))

        data = OrderedDict(data)

        if to_json:
            return json.dumps(data, indent=4)
        else:
            return data


class Location:

    """docstring for location"""

    def __init__(self, latitude, longitude, elevation, timezone):
        super().__init__()
        self.longitude = longitude
        self.latitude = latitude
        self.elevation = elevation
        self.timezone = timezone


class Date:

    def __init__(self, year, month, day):
        super().__init__()
        self.year = year
        self.month = month
        self.day = day

if __name__ == '__main__':
    # location = Location(48.87, 2.66, 32.54, 'Europe/Paris')

    location = Location(31.76832, 35.21371, 779.46, 'Asia/Jerusalem')

    date = Date(2015, 1, 4)

    t = Time(date, location)
    zman = Zmanim(t, location)
    print(zman.get_data(to_local=True, to_json=True))
