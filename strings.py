#!/usr/bin/python

import datelib


def weekday_name(weekday):

    week = ["Sunday", "Monday", "Tuesday", "Wednesday",
            "Thursday", "Friday", "Shabbat"]
    return week[weekday]


def gregorian_month_name(month):

    if month == 1:
        return "January"
    elif month == 2:
        return "February"
    elif month == 3:
        return "March"
    elif month == 4:
        return "April"
    elif month == 5:
        return "May"
    elif month == 6:
        return "June"
    elif month == 7:
        return "July"
    elif month == 8:
        return "August"
    elif month == 9:
        return "September"
    elif month == 10:
        return "October"
    elif month == 11:
        return "November"
    elif month == 12:
        return "December"


def jewish_month_name(month, year):

    if month == 1:
        return "Nissan"
    elif month == 2:
        return "Iyar"
    elif month == 3:
        return "Sivan"
    elif month == 4:
        return "Tammouz"
    elif month == 5:
        return "Av"
    elif month == 6:
        return "Eloul"
    elif month == 7:
        return "Tishri"
    elif month == 8:
        return "Heshvan"
    elif month == 9:
        return "Kislev"
    elif month == 10:
        return "Tevet"
    elif month == 11:
        return "Shevat"
    elif month == 12:
        if datelib.hebrew_leap(year):
            return "Adar I"
        else:
            return "Adar"
    elif month == 13:
        return "Adar II"


def omer(omer_day):
    return str(omer_day) + " LaOmer"


def paracha_name(pos):
    parachiot = ["Berechit", "Noa'h", "Le'h Le'ha", "Vayera", "'Hayei Sara",
                 "Toldot", "Vayetsei", "Vayishla'h", "Vayechev", "Miketz",
                 "Vayigach", "Vaye'hi", "Chemot", "Vaera", "Bo", "Bechala'h",
                 "Yitro", "Michpatim", "Teroumah", "Tetsaveh", "Ki Tissa",
                 "Vayakel", "Pekoudei", "Vayikra", "Tsav", "Chemini", "Tazria",
                 "Bamidbar", "Nasso", "Beha'alot'ha", "Shelach", "Kora'h",
                 "'Houkat", "Balak", "Pin'has", "Matot", "Massei", "Devarim",
                 "Vaet'hanan", "Eikev", "Re'eh", "Choftim", "Ki Tetse",
                 "Ki Tavo", "Nitsavim", "Vayele'h", "Ha'azinou",
                 "Vayakel Pekoudei", "Tazria Metsora", "A'hrei Mot Kedochim",
                 "Behar Be'houkotai", "'Houkat Balak", "Matot Massei",
                 "Nitsavim Vayele'h"]
    return parachiot[pos]
