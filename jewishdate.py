#!/usr/bin/python

# Furnish a JewishDate class
from collections import OrderedDict
import json

import arrow

import datelib
import holidays
import torah
import zmanim
import location
import strings


# TODO: add methods for month / year
class HebrewDate():

    # def __init__(self, **kwargs)
    def __init__(self, h_year, h_month, h_day, loc=None):
        self._hebrew = None
        self._gregorian = None
        self.hebrew = (h_year, h_month, h_day)

        self.location = None
        # if isinstance(loc, location.Location):
        #     self.location = loc
        # else:
        #     raise TypeError("location must be an instance of Location")
        # pass

    @classmethod
    def from_gregorian(cls, g_year, g_month, g_day):
        """ Create a new HebrewDate from a gregorian date """
        y, m, d = datelib.gregorian_to_hebrew(g_year, g_month, g_day)
        return cls(y, m, d)

    def get_zmanim(self):
        """ Get zmanim for current day """
        if self.location is None:
            raise ValueError("No location given")
            return None
        year, month, day = datelib.hebrew_to_gregorian(self.h_year,
                                                       self.h_month,
                                                       self.h_day)
        zman = zmanim.Zmanim(self, self.location)

        return zman.get_data()

    @property
    def gregorian(self):
        """ Get current gregorian date """
        g = self._gregorian
        return (g.year, g.month, g.day)

    @gregorian.setter
    def gregorian(self, date):
        """ Set an hebrew date and update gregorian date
        Take a tuple (gregorian year, gregorian month, gregorian day)
        """
        self._hebrew = datelib.gregorian_to_hebrew(date[0], date[1], date[2])
        self._gregorian = arrow.Arrow(date[0], date[1], date[2])

    @property
    def hebrew(self):
        """ Get current hebrew date """
        return self._hebrew

    @hebrew.setter
    def hebrew(self, date):
        """ Set an hebrew date and update gregorian date
        Take a tuple (hebrew year, hebrew month, hebrew day)
        """

        self._hebrew = (date[0], date[1], date[2])
        y, m, d = datelib.hebrew_to_gregorian(date[0],
                                              date[1],
                                              date[2])
        self._gregorian = arrow.Arrow(y, m, d)

    @property
    def holiday(self, diaspora=False, modern=False):
        """ Get holiday for current day """
        return holidays.get_holiday(self._hebrew[0],
                                    self._hebrew[1],
                                    self._hebrew[2],
                                    diaspora,
                                    modern)

    @property
    def parasha(self, in_israel=True):
        """ Get parasha for current day """
        return torah.get_parasha(self._hebrew[0], self._hebrew[1],
                                 self._hebrew[2], in_israel)

    def get_data(self, date=True, parasha=True, holiday=True, zmanim=True,
                 to_json=True):
        data = ()
        if date:
            data += (('hebrew', self.hebrew), ('gregorian', self.gregorian))
        if parasha:
            data += (('parasha', self.parasha))
        if holiday:
            data += (('holiday', self.holiday))
        if zmanim:
            data += (('zmanim'), zmanim.Zmanim)

        data = OrderedDict(data)

        if to_json:
            return json.dumps(data, indent=4)
        else:
            return data

    def __str__(self):
        month = strings.jewish_month_name(self._hebrew[1], self._hebrew[0])
        return "{} {} {} - {}".format(self._hebrew[2],
                                      month,
                                      self._hebrew[0],
                                      self._gregorian.format('DD MMMM YYYY'))

    # Internals

    ## Validators, not used now

    def _validator_hebrew(self, h_year, h_month, h_day):
        """ Validate an hebrew date """
        hym = datelib.hebrew_year_months(h_year, h_month)
        if h_month < 1 or h_month > hym:
            raise ValueError('Hebrew month has to be between' +
                             '1 and 12 (13 for leap year) ')

        hmd = datelib.hebrew_month_days(h_year, h_month)
        if h_day < 1 or h_day > hmd:
            raise ValueError('Hebrew day must be 1 < d < 30')

    def _validator_gregorian(self, g_year, g_month, g_day):
        """ Validate a gregorian date """
        if g_month < 1 and g_month > 12:
            raise ValueError('Gregorians month has to be between' +
                             '1 and 12')
        if g_day < 1:
            raise ValueError('Gergorian day of month must be > 1')
        if g_year < 1:
            raise ValueError('Gregorian date < 1 can\'t be calculated')

if __name__ == '__main__':
    g = HebrewDate(5775, 10, 17)
    print("greg 2: ", g.gregorian)

    d = HebrewDate.from_gregorian(2015, 1, 10)

    print("str:", d)
    #print(d.get_zmanim())
    print("heb:", d.hebrew)
    print("greg:", d.gregorian)
    print("holiday:", d.holiday)
    print("parasha:", d.parasha)
